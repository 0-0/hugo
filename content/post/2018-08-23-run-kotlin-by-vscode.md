---
title: "在VSCode中编写Kotlin"
date: 2018-08-23
tag: ["kotlin", "技术"]
---


在 Mac 上，不想用 IDEA 来写 Kotlin，太吃内存，而且用 VS Code 很顺手了，就想用它来打造 Kotlin 的编辑器。

### 安装命令行编译器

通过 Homebrew 的方式安装

```bash
brew update
brew install kotlin
```

### 安装扩展

要让 VS Code 支持 Kotlin，需要在内置的应用商店安装两个扩展。

[Kotlin Language ](https://marketplace.visualstudio.com/items?itemName=mathiasfrohlich.Kotlin) 这个扩展为 Kotlin 的语法高亮提供支持。

安装步骤如下：

1. 打开 VS Code
2. 按下 「 Shift + Cmd + X 」 切换到扩展面板
3. 在搜索框，输入 Kotlin 关键字，选中 Kotlin Language 安装
4. 点击「重新加载」后扩展就生效了

![Kotlin 扩展!](http://ojpkcr4e8.bkt.clouddn.com/15350335278916.png)

[Code Runner](https://marketplace.visualstudio.com/items?itemName=formulahendry.code-runner) 是我们需要安装的第二个扩展，这个扩展的支持三十多种程序语言的运行，当然也包括 Kotlin 这门语言。
![Code Runner 扩展](http://ojpkcr4e8.bkt.clouddn.com/15350335524270.png)


下面来介绍一下这家伙怎么使用。

1. Cmd + N 新建代码文件
2. 输入以下代码     
```kotlin
fun main(args: Array<String>){  
    println("test")
}
```

3. 按 Cmd + S 以 **.kt** 为后缀名保存在本地
![test.kt 文件](http://ojpkcr4e8.bkt.clouddn.com/15350336004746.png)

4. 右键点击这个文件选择 「 Run Code 」
![运行 Kotlin 代码](http://ojpkcr4e8.bkt.clouddn.com/15350336156211.png)

5. 输出就在下面的窗口中显示出来   
![输出面板](http://ojpkcr4e8.bkt.clouddn.com/15350337801944.png)


那么，Code Runner 干了些啥让代码运行起来了呢

```bash
cd "/Users/workspace/" && \
kotlinc tempCodeRunnerFile.kt -include-runtime -d \
tempCodeRunnerFile.jar && java -jar tempCodeRunnerFile.jar
```

输出里有显示整个编译运行的命令行。

用 VS Code 学习 Kotlin 高效快速，赶快来试试吧。 👏


