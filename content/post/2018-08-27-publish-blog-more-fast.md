---
title: 使用脚本发布静态网站的终极方案
tags: ["shell" , "git" , "alfred"]
layout: post
categories: ["shell" , "git"]
date: 2018-08-27
---

>  使用 Github Page 托管的静态博客，每次更新的时候都要进行一次提交。在文章完成的时候，还要输入繁琐的命令才能博客发布出去，对于这样繁琐的事情为什么不能用自动化的方式交给机器完成呢？让计算机自动地完成这些任务，既能解放双手又能让我们的大脑解放出来用在重要的事情上，岂不美哉。通过 shell 脚本就能完成，不会像之前那样文章完成后还要去敲命令进行发布。

<!-- more -->

## 准备工作

使用 git 发布到 github pages 的博客已部署好，进入本地博客目录。如果你还没配置好，参考这里：https://pages.github.com 。

##  配置发布脚本

这个脚本的作用就是简化之前的繁琐的 git 提交，让你执行一个命令就可以进行博客的提交发布。

我这里使用的分支是 **master**，如果无法正常发布到 github ，你还可以使用代理

```bash
#!/bin/sh

export https_proxy=http://127.0.0.1:1080;export http_proxy=http://127.0.0.1:1080
cd ~/Blog/hugo
git config user.name "your name"
git config user.email "your email"
commit=`date "+%Y-%m-%d %H:%M:%S" `
git add --ignore-removal .
git commit -m "$1 @ $commit "
git push -u origin master
echo "complete commit :)"
exit
```
脚本中的代理，路径 user.name 和 user.email 替换成你自己本地的配置，修改后，你就能愉快地使用了。当你将博客文章写好后，执行 `~/publog.sh 'commit记录'` ，commit记录就是脚本中的 **$1** 。别忘了，这条命令还要赋予执行权限 `chmod +x publog.sh` 。

## 使用 alias 简化记忆

> 这条路径每次都要输入一遍，真的也还是麻烦，况且我也不能轻松记住它。

用 alias 来简写命令，你可以用自然的别名方式简化记忆命令。例如：

```bash
alias gblog='~/publog.sh'
```
将这句加入到 ~/.zshrc 或 ~/.bashrc 中，然后执行 `source ~/.zshrc` 或 `source ~/.bashrc` 让其生效。

提交的时候用 `gblog 'commit记录'` 这条命令就好记的多了，你也可以将 gblog 改成其它你容易记住的别名。

## 使用 alfred 加快工作流

快捷键打开 alfred ，如下图：

![工作流](http://ojpkcr4e8.bkt.clouddn.com/15323305141276.jpg)

本文就是采用这种方案发布的。 Enjoy yourself  :)