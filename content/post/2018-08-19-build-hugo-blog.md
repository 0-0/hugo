---
title: "Hugo快速搭建博客"
date: 2018-08-19
tag: ["hugo", "随笔"]
bigimg: [{src: "/img/370884_ea6b.jpg"}]
---

Hugo 是基于 Go 语言的静态网站生成器，基于 Go语言的高效，可以快速生成博客。同 Jekyll 和 Hexo 相比，在转换成百上千的 markdown 文档时的效率要高出至少几倍。

### 安装

`brew install hugo`   

### 版本查询

`hugo version` 

### **命令帮助**

`hugo help` 

### **本地创建网站**

`hugo new site XXX` 

Hugo 已经在 XXX 目录下自动生成好了一个网站模版，接下来你需要一个博客主题。官方的主题站点 [链接](https://themes.gohugo.io/)，找到你喜欢的主题，复制下主题的 repo 地址。

### 克隆一个博客主题

```
cd XXX/themes
git clone --recursive https://github.com/halogenica/beautifulhugo.git
```

### 新建一个页面或是博客文章

```
# 新建一个页面
hugo new about.md 
# 新建一片博客文章
hugo new post/2018-08-18-first.md
```

在文件里写下你的文章，注意的是页面和博客文章是两个概念。页面是 About 之类的单独页面，区别于博客文章。博客文章的写法类似于 jekyll ，有些博客主题有自定义的**模板变量**，官方使用说明要仔细看看。

### 本地运行你的博客

回到 XXX 目录下，运行以下命令。

```
hugo server --theme=beautifulhugo --buildDrafts --watch
```

运行成功后，按照命令行提示打开 localhost:1313 即可看到效果。Hugo 的优秀之处在于它能**热加载**，你本地修改保存后，它能马上重新渲染出来。

当你想换个主题的时候，只需要将本地的主题通过 --theme 参数切换即可。你只需要**关注于当前的写作**，不用去在意主题，样式，这些都可以在以后来弄。

### 博客配置

**config.toml** 是博客的配置文件，可以配置标题，网站统计，评论系统等。博客主题的 repo 一般都会有说明，按照官方的指引配置就能得到你想要的结果。

### 静态博客本地生成

上面的配置文件是 Hugo 作者创建的，当然你也可以用 YAML 的规范来写，只是写法不同。博客的生成很简单，`Hugo`  一个命令就搞定来，接着你的博客目录下会多出一个 **public** 的文件夹，里面存放的就是生成博客的静态文件。你可以将这个文件夹部署到你的服务器上，或是你的 Github Page 上，网址的跟路径要和 **config.toml** 中的 **baseurl** 配置对应。

### 总结：

静态网站生成工具有很多，细细说来我用过 Jekyll、Hexo、GitBook…… Hugo 作为后起之秀，性能很优秀且支持热加载，对于新手来说，上手的难度也不大。对于写博客这件事来说，重要的是**屏幕后的那个脑袋有没有将思考的内容输出**，而不是花精力在改主题代码，折腾工具上面。 


